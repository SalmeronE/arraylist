/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.edu.utr.datastructure.st1536;

import mx.edu.utr.datastructures.List;

/**
 *
 * @author Daniela
 */
public class ArrayList implements List {

    private Object[] elements;
    private int size;

    public ArrayList() {

    }

    public ArrayList(int initialCapacity) {
        elements = new Object[initialCapacity];
    }

    @Override
    public boolean add(Object element) {
        ensureCapacity(size + 1);
        elements[size++] = element;
        return true;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        if (size == 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Object get(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException();
        }
        return elements[index];
    }
//    
//    public Object set (int index, Object element) {
//        if (index >= size) {
//        Object old = elements [index];
//        elements[index] = element;
//         return old;
//        }
//    }

    private void ensureCapacity(int minCapacity) {
        int oldCapacity = elements.length;
        if (minCapacity > oldCapacity) {
            int newCapacity = oldCapacity * 2;
            Object[] nelements;
            nelements = new Object[newCapacity];
            for (int x = 0; x <= oldCapacity; x++) {
                nelements[x] = elements[x];
            }
        }
    }

    @Override
    public void add(int index, Object element) {
        rangeCheck(index);
        ensureCapacity(size + 1);
        elements[size++] = element;
        size++;
    }

    @Override
    public void clear() {
        for (int i = 0; i < elements.length; i++) {
            elements[i] = 0;
        }
    }

    @Override
    public int indexOf(Object o) {
        for (int i = 0; i < elements.length; i++) {

            if (elements[i] == o) {
                System.out.println(i);
            }
        }

        return -1;
    }

    @Override
    public Object remove(int index) {
        Object oldElement = elements[index];
        int numberMoved = size - index - 1;
        if (numberMoved > 0) {
            System.arraycopy(elements, index + 1, elements, index, numberMoved);
        }
        elements[--size] = null;
        return oldElement;

    }

    @Override
    public Object set(int index, Object element) {
        if (index >= size) {
            throw new IndexOutOfBoundsException();
        } else {
            Object old = elements[index];
            elements[index] = elements;
            return old;
        }
    }

    private void rangeCheck(int index) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException();
        }
    }

}
